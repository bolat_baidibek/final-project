package mywebfinalproj;

import org.springframework.data.repository.CrudRepository;

public interface UploadRepo extends CrudRepository<UploadFile,Long> {
    UploadFile findByName(String name);
    Iterable<UploadFile> findByType(String type);
}
